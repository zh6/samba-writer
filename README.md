## Samba writer

This component can be used for creating an XML document and uploaded to desired destination for to be use by Samba automation tool.

Access the component via this URL (you need to specify the stack and your project ID):
https://connection.{STACK}.keboola.com/admin/projects/{Project_ID}/extractors/zh6.wr-samba

Link to Samba API documentation: </br>
https://doc.samba.ai/knowledge-base/datove-feedy/?lang=en#customers-feed

### Contact

In case of any issues or problems with this component don't hesitate to contact me on this mail: **zdenekhanik6@gmail.com**