## Samba.ai writer

Based on the input table this component builds an XML document
which will then be uploaded to specified destination using
an SFTP writer. Set the connection parameters in the last
section of this component.

Below please set the mapping for each Samba parameter 
based on the [documentation](https://doc.samba.ai/knowledge-base/datove-feedy/?lang=en) 
and your table input columns. Nested tags need to be created allready in the input table.

For example for the Orders feed you can create the `ITEMS` tags like this:
```
SELECT
	"order_id",
	LISTAGG('<ITEM><PRODUCT_ID>' || "id"  || '</PRODUCT_ID>'
		|| '<AMOUNT>' || "quantity_ordered"  || '</AMOUNT>'
		|| '<PRICE>' || "sell_price" || '</PRICE></ITEM>') AS "items"
FROM "order_item"
GROUP BY "order_id"
```

Or for the Customers feed like this:
```
SELECT
	"customer_id",
	'<PARAMETER><NAME>SEGMENT</NAME><VALUE>' || "segment" || '</VALUE></PARAMETER>'
	||
	'<PARAMETER><NAME>LAST_ORDER</NAME><VALUE>' || "last_order_id" || '</VALUE></PARAMETER>'
	||
	'<PARAMETER><NAME>LAST_SERVICE</NAME><VALUE>' || "last_service" || '</VALUE></PARAMETER>'
	AS "parameters"
FROM "customers"
```

---
component version: *0.3.0*