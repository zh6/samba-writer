'''
Template Component main class.

'''
import logging
import pandas as pd
import pysftp
from datetime import datetime, timedelta
import pytz

from keboola.component.base import ComponentBase
from keboola.component.exceptions import UserException

# configuration variables
KEY_API_TOKEN = '#api_token'
KEY_PRINT_HELLO = 'print_hello'

# list of mandatory parameters => if some is missing,
# component will fail with readable message on initialization.
REQUIRED_PARAMETERS = [KEY_PRINT_HELLO]
REQUIRED_IMAGE_PARS = []


class Component(ComponentBase):
    """
        Extends base class for general Python components. Initializes the CommonInterface
        and performs configuration validation.

        For easier debugging the data folder is picked up by default from `../data` path,
        relative to working directory.

        If `debug` parameter is present in the `config.json`, the default logger is set to verbose DEBUG mode.
    """

    def __init__(self):
        super().__init__()

    def run(self):
        '''
        Main execution code
        '''

        # TODO: Better component description
        # TODO: explore other SFTP writers
        # TODO: check connection status, timeout
        # TODO: add incremental number to the filename using self.write_state_file
        # TODO: Raise error when no input table is provided

        logging.info('Running...')

        FEEDS = {"Customers": ('<CUSTOMERS>', '<CUSTOMER>', '</CUSTOMER>', '</CUSTOMERS>'),
                 "Orders": ('<ORDERS>', '<ORDER>', '</ORDER>', '</ORDERS>'),
                 "Products": ('<PRODUCTS>', '<PRODUCT>', '</PRODUCT>', '</PRODUCTS>')}

        # config.json parameters
        CONFIG = self.configuration.config_data
        PARAMS = self.configuration.parameters

        # read data and creating pandas dataframe
        INPUT_FILE_PATH = self.get_input_tables_definitions()[0].full_path
        data = pd.read_csv(INPUT_FILE_PATH, encoding='utf-8', keep_default_na=False, dtype=str)

        # output file informations
        # if outputfile is provided by the user - use it, otherwise create a temp file
        # then use the temp file to stream data localy and load to sftp
        try:
            output_file_name = CONFIG["storage"]["output"]["files"][0]["source"]
        except (KeyError, IndexError):
            logging.warning("Output file for writing back to Storage not set. Creating a temporary file.")
            output_file_name = 'temporary_xml_for_streaming.xml'

        output_file_path = self.files_out_path + '/' + output_file_name
        # print mappings
        # logging.warning(self.configuration.tables_input_mapping)
        # logging.warning(self.configuration.files_output_mapping)

        # checking if all columns exist in the input file
        columns = [c["table_col"] for c in PARAMS["feed_parameters"]]

        for c in columns:
            if c not in data.columns:
                raise UserException(f'Column "{c}" doesn\'t exist in the input table')

        # writing file localy
        with open(output_file_path, "w") as outfile:
            outfile.write('<?xml version="1.0" encoding="utf-8" ?>\n'
                          + FEEDS[PARAMS["feed_type"]][0])

            for i, row in data.iterrows():
                outfile.write(FEEDS[PARAMS["feed_type"]][1])

                # all parameters (including nested parameters created in the DB)
                for item in PARAMS["feed_parameters"]:
                    if item["ignore_empty"] and str(row[item["table_col"]]) == '':
                        continue
                    else:
                        outfile.write('<' + str(item["param_name"]) + '>'
                                      + str(row[item["table_col"]])
                                      + '</' + str(item["param_name"]) + '>')

                outfile.write(FEEDS[PARAMS["feed_type"]][2])
            outfile.write(FEEDS[PARAMS["feed_type"]][3])

        # creating output file manifest file
        out_file_def = self.create_out_file_definition(name=output_file_path)
        self.write_manifest(out_file_def)

        # checking input
        if PARAMS["connection_config"]["hostname"] == '':
            raise UserException('Incomplete connection configuration. Missing host name.')

        if PARAMS["connection_config"]["file_name"] == '':
            raise UserException('Incomplete connection configuration. Missing file name.')

        if PARAMS["connection_config"]["append_date"]:
            if PARAMS["connection_config"]["date_format"] == '':
                raise UserException('Incomplete connection configuration. Missing date format.')

        if PARAMS["connection_config"]["port"] == '':
            raise UserException('Incomplete connection configuration. Missing port number.')

        if PARAMS["connection_config"]["username"] == '':
            raise UserException('Incomplete connection configuration. Missing user name.')

        if PARAMS["connection_config"]["password"] == '':
            raise UserException('Incomplete connection configuration. Missing password.')

        temp_file_name = PARAMS["connection_config"]["file_name"].strip()

        # output file name including date if selected
        if PARAMS["connection_config"]["append_date"]:
            if PARAMS["connection_config"]["date_format"] == "Year-Month-Day (yesterday)":
                date_format = (datetime.now(pytz.timezone('Europe/Prague')) + timedelta(days=-1)).strftime("%Y-%m-%d")
            elif PARAMS["connection_config"]["date_format"] == "Year-Month-Day_HoursMinutesSeconds (today)":
                date_format = datetime.now(pytz.timezone('Europe/Prague')).strftime("%Y-%m-%d_%H%M%S")
            else:
                date_format = datetime.now(pytz.timezone('Europe/Prague')).strftime("%Y-%m-%d")

            if temp_file_name[-4:] == '.xml':
                sftp_out_file_name = temp_file_name[:-4] + date_format + '.xml'
            else:
                sftp_out_file_name = temp_file_name + date_format + '.xml'

        else:
            sftp_out_file_name = temp_file_name

        cnopts = pysftp.CnOpts()
        cnopts.hostkeys = None
        with pysftp.Connection(host=PARAMS["connection_config"]["hostname"],
                               username=PARAMS["connection_config"]["username"],
                               password=PARAMS["connection_config"]["password"],
                               port=PARAMS["connection_config"]["port"],
                               cnopts=cnopts) as sftp:
            sftp.put(output_file_path, sftp_out_file_name, confirm=False)

        # Write new state - will be available next run
        # don't need to use this now.
        # self.write_state_file({"some_state_parameter": "value"})


"""
        Main entrypoint
"""
if __name__ == "__main__":
    try:
        comp = Component()
        # this triggers the run method by default and is controlled by the configuration.action parameter
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
